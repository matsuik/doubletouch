﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsoleteTouchControllerAgent : Agent {

	private bool previousIsTouching;

	private Vector2 previousTouchPosition;

	private int widthNNOutput;
	private int heightNNOutput;

	[HideInInspector]
	public Touch[] touchesFromBrain;

	[HideInInspector]
	public int updateCount = 0;

	[HideInInspector]
	public int fixedUpdateCount = 0;

	public GameObject player;

	public TouchController touchController;

	public override void OnEnable ()
	{
		base.OnEnable ();
//		touchController.isHuman = false;
	}

	public override List<float> CollectState()
	{	
		var state = new List<float> ();
		if (touchesFromBrain.Length == 0) {
			state = new List<float> () {
				Screen.width,
				Screen.height,
				widthNNOutput,
				heightNNOutput,
				touchesFromBrain.Length,
				0,
				Time.time,
				0,
			};
		} else {
			Vector2 touchWorldPosition = Camera.main.ScreenToWorldPoint (touchesFromBrain [0].position);
			state = new List<float> () {
				Screen.width,
				Screen.height,
				widthNNOutput,
				heightNNOutput,
				touchWorldPosition.x,
				touchWorldPosition.y,
				Time.time,
				0
			};
			
		}
		updateCount = 0;
		fixedUpdateCount = 0;
		return state;
	}

	public override void AgentReset()
	{
		previousIsTouching = false;
		previousTouchPosition = Vector2.zero;

		widthNNOutput = brain.brainParameters.cameraResolutions [0].width;
		heightNNOutput = brain.brainParameters.cameraResolutions [0].height;

		touchesFromBrain = new Touch[] {new Touch()};

		updateCount = 0;
		fixedUpdateCount = 0;

		player.transform.position = new Vector3 (1f, -1f, 0);
	}

	public override void AgentStep(float[] act)
	{
		var tmp = (int)act [0];
		bool isTouching = System.Convert.ToBoolean (tmp);
		if (previousIsTouching == false && isTouching == false) { // 全くtouchしてない
			touchesFromBrain = new Touch[0];
			return;
		}

		int index1DLowRes = (int)act [1];

		int heightInt = index1DLowRes / widthNNOutput;
		heightInt = heightNNOutput - heightInt; // numpyのindexingとtouch positionの縦軸が逆
		float heightFloat = heightInt * ((float)Screen.height / (float)heightNNOutput);

		int widthInt = index1DLowRes % widthNNOutput;
		float widthFloat = widthInt * ((float)Screen.width / (float)widthNNOutput);

		Touch touch = new Touch ();
		touch.position = new Vector2 (widthFloat, heightFloat);
		touch.fingerId = 1912;
		touch.deltaPosition = touch.position - previousTouchPosition;

		if (previousIsTouching == false && isTouching == true) {
			touch.phase = TouchPhase.Began;
			touch.deltaPosition = Vector2.zero;
		} else if (previousIsTouching == true && isTouching == true) {
			touch.phase = TouchPhase.Moved;
		} else if (previousIsTouching == true && isTouching == false) {
			touch.phase = TouchPhase.Ended;
		}

		touchController.repeatInput = null;
		touchController.inputQueue.Enqueue (touch);

		previousIsTouching = isTouching;
		previousTouchPosition = new Vector2 (widthFloat, heightFloat);
	}

	public override void AgentOnDone()
	{

	}
}
