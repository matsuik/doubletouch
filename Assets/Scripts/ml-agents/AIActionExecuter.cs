﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionExecuter : MonoBehaviour
{
    [HideInInspector]
    public Queue<Touch> touchQueue;
    private PlayerController pc;

    private Vector2 touchPosition;

    void Awake()
    {
        pc = GetComponent<PlayerController>();
        touchQueue = new Queue<Touch>();
        touchPosition = Vector2.zero;
    }

    void FixedUpdate()
    {
        if (!pc.isTouched)
        {
            pc.OnTouchBegan(new Touch());
            touchPosition = Vector2.zero;
        }
        else if (touchQueue.Count > 0)
        {
            if (touchQueue.Peek().phase == TouchPhase.Moved)
            {
                Touch touch = touchQueue.Peek();
                touchPosition += touch.deltaPosition;
                touch.position = touchPosition;
                pc.OnTouchMoved(touch);
            }
            else
            {
                Touch touch = touchQueue.Dequeue();
                touchPosition += touch.deltaPosition;
                touch.position = touchPosition;
                pc.OnTouchEnded(touch);
            }
        }
    }

}
