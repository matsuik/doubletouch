﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AIGameController : MonoBehaviour
{
    public bool friendlyFire;

    private GameObject restartButton;
    public bool isLogEnabled = false;
    public Academy academy;
    // Use this for initialization
    public static AIGameController gameController;

    void Awake()
    {
        gameController = this;
    }
    void Start()
    {
        var buttonName = "GameUI/Canvas/AI Restart Button";
        restartButton = GameObject.Find(buttonName);
        if (restartButton == null)
        {
            Debug.LogError($"CANNOT FIND {buttonName}");
        }
        restartButton.SetActive(false);
    }

    public void GameOver()
    {
        if (isLogEnabled) Debug.Log("GameOver");
        restartButton.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (this.isActiveAndEnabled)
        {
            if (isLogEnabled) Debug.Log("boundary exit");

            other.gameObject.SetActive(false);

            if (other.CompareTag("Player"))
            {

                other.GetComponent<PlayerController>().isFinished = true;
                if (academy != null && !academy.isInference)
                {
                    var agent = other.gameObject.GetComponent<PlayerController>().agent;
                    if (agent != null)
                    {
                        agent.DestructionReward(other.gameObject.GetComponent<PlayerController>());
                    }
                }
                else
                {
                    GameOver();
                }
            }
        }
    }
}
