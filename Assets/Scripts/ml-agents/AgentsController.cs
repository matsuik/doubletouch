﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentsController : Agent
{
    public GameObject opponentGo;

    private PlayerHolder opponentHolder;

    private AgentsController opponentAgentController;

    private PlayerHolder myHolder;

    private AIActionExecuter jonyExecuter;
    private AIActionExecuter krossExecuter;

    private List<Vector2> deltaPositionList;

    private const float leftEdgeX = -4.8f;
    private const float rightEdgeX = 4.5f;
    private const float groundHeightY = -1.0f;

    private int frameToSkip;

    private int i_frame;

    public bool logEnabled;


    public override void InitializeAgent()
    {
        frameToSkip = GameObject.Find("AzbilAcademy").GetComponent<AzbilAcademy>().frameToSkip;
        i_frame = 0;

        myHolder = GetComponent<PlayerHolder>();
        jonyExecuter = myHolder.jonyGo.GetComponent<AIActionExecuter>();
        krossExecuter = myHolder.krossGo.GetComponent<AIActionExecuter>();

        opponentHolder = opponentGo.GetComponent<PlayerHolder>();
        opponentAgentController = opponentGo.GetComponent<AgentsController>();

        float s = 40f / frameToSkip;
        float l = Mathf.Clamp(150f / frameToSkip, 30f, 200f);

        deltaPositionList = new List<Vector2>()
        {
            Vector2.zero, Vector2.zero,
            Vector2.right * s, Vector2.right * l, // 0
			Vector2.right * s + Vector2.up * s, Vector2.right * l + Vector2.up * l, //45
			Vector2.up * s, Vector2.up * l, //90
			-Vector2.right * s + Vector2.up * s, -Vector2.right * l + Vector2.up * l, // 135
			-Vector2.right * s, -Vector2.right * l, // 180
			-Vector2.right * s + -Vector2.up * s, -Vector2.right * l + -Vector2.up * l, // 225
			-Vector2.up * s, -Vector2.up * l, // 270
			Vector2.right * s + -Vector2.up * s, Vector2.right * l + -Vector2.up * l // 315
		};

    }

    public override void AgentReset()
    {
        if (logEnabled) Debug.Log("AgentReset");

        myHolder.jonyGo.SetActive(true);
        myHolder.jonyPc.Initialize();
        myHolder.jonyGo.transform.position = new Vector3(UnityEngine.Random.Range(-7f, 7f), UnityEngine.Random.Range(-1f, 4f), 0f);
        if (UnityEngine.Random.value > 0.5)
        {
            myHolder.jonyGo.transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
        }
        else
        {
            myHolder.jonyGo.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        }

        myHolder.krossGo.SetActive(true);
        myHolder.krossPc.Initialize();
        myHolder.krossGo.transform.position = new Vector3(UnityEngine.Random.Range(-7f, 7f), UnityEngine.Random.Range(-1f, 4f), 0f);
        if (UnityEngine.Random.value > 0.5)
        {
            myHolder.krossGo.transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
        }
        else
        {
            myHolder.krossGo.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        }
    }

    public override List<float> CollectState()
    {
        if (logEnabled) Debug.Log("CollectState");
        // (7 + 14 + 7 + 22) * 2 = 100
        List<float> state = new List<float>();
        state.AddRange(CommonFeatureFromGo(myHolder.jonyGo));
        state.AddRange(JonyFeatureFromPC(myHolder.jonyPc));

        state.AddRange(CommonFeatureFromGo(opponentHolder.jonyGo));
        state.AddRange(JonyFeatureFromPC(opponentHolder.jonyPc));

        state.AddRange(CommonFeatureFromGo(myHolder.krossGo));
        state.AddRange(KrossFeatureFromPC((Kross)myHolder.krossPc));

        state.AddRange(CommonFeatureFromGo(opponentHolder.krossGo));
        state.AddRange(KrossFeatureFromPC((Kross)opponentHolder.krossPc));

        return state;
    }

    public override void AgentStep(float[] act)
    {
        if (logEnabled) Debug.Log("AgentStep");
        if (logEnabled) Debug.Log("time: " + Time.time);

        for (int i = 0; i < act.Length; i++)
        {
            if (logEnabled) Debug.Log("act " + i + ": " + act[i]);
        }

        if (i_frame == frameToSkip + 1)
        {
            i_frame = 0;
        }
        if (i_frame == 0)
        {
            jonyExecuter.touchQueue.Clear();
            jonyExecuter.touchQueue.Enqueue(IntToTouch((int)act[0]));

            krossExecuter.touchQueue.Clear();
            krossExecuter.touchQueue.Enqueue(IntToTouch((int)act[1]));
        }
        i_frame++;

        if (!myHolder.jonyGo.activeInHierarchy && !myHolder.krossGo.activeInHierarchy)
        {
            if (logEnabled) Debug.Log("done = true");
            done = true;
        }
    }

    private Touch IntToTouch(int i_act)
    {
        Touch touch = new Touch();
        if (i_act == 1)
        {
            touch.phase = TouchPhase.Ended;
        }
        else
        {
            touch.phase = TouchPhase.Moved;
        }
        touch.deltaPosition = deltaPositionList[i_act];

        return touch;
    }

    private List<float> CommonFeatureFromGo(GameObject go)
    {
        List<float> state;
        if (go.activeInHierarchy)
        {   // 7
            state = new List<float>(){
            BoolToPlusMinus(go.activeInHierarchy),
            go.transform.position.x / 7f,
            Mathf.Clamp((go.transform.position.x - leftEdgeX) / 7f, (go.transform.position.x - leftEdgeX) / 7f, 0f),
            Mathf.Clamp((go.transform.position.x - rightEdgeX) / 7f, 0f, (go.transform.position.x - rightEdgeX) / 7f),
            go.transform.position.y / 5f,
            (go.transform.position.y - groundHeightY) / 5f,
            BoolToPlusMinus(go.transform.transform.rotation.eulerAngles.y == 0f),
            };
        }
        else
        {
            state = new List<float>(new float[7]);
            state[0] = BoolToPlusMinus(go.activeInHierarchy);
        }
        return state;
    }

    private List<float> CommonFeatureFromPC(PlayerController myPc)
    {
        List<float> state;
        // 12 = 9 + 3
        if (myPc.isActiveAndEnabled)
        {
            state = new List<float>(){
            myPc.velocity.x / 20f,
            myPc.velocity.y / 20f,
            myPc.cumulativeDamage / 200f,
            BoolToPlusMinus(myPc.canMove),
            BoolToPlusMinus(myPc.delayingDoubleJump),
            BoolToPlusMinus(myPc.hasFastFallen),
            BoolToPlusMinus(myPc.isAttacking),
            BoolToPlusMinus(myPc.grounded),
            BoolToPlusMinus(myPc.animator.GetBool(myPc.guardAnimatorParamHash))
        };
            state.AddRange(IntToOneHotVector(myPc.numPossibleDoubleJump, 3));
        }
        else
        {
            state = new List<float>(new float[12]);
        }

        return state;
    }

    private List<float> JonyFeatureFromPC(PlayerController myPc)
    {
        // 12 + 2 = 14

        List<float> state;
        if (myPc.isActiveAndEnabled)
        {
            state = new List<float>();
            state.AddRange(CommonFeatureFromPC(myPc));
            state.Add(BoolToPlusMinus(myPc.animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Jab")));
            state.Add(BoolToPlusMinus(myPc.animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Smash")));
            return state;
        }
        else
        {
            state = new List<float>(new float[14]);
        }

        return state;
    }

    private List<float> KrossFeatureFromPC(Kross kross)
    {
        List<float> state;
        // 12 + 5 + 3 + 2 = 22
        if (kross.isActiveAndEnabled)
        {
            state = new List<float>();
            state.AddRange(CommonFeatureFromPC(kross));

            if (kross.currentArrow != null)
            {
                ArrowController tmpAc = kross.currentArrow.GetComponent<ArrowController>();
                state.AddRange(new List<float>(){
                    1f,
                    kross.currentArrow.transform.position.x,
                    kross.currentArrow.transform.position.y,
                    tmpAc.velocity.x,
                    tmpAc.velocity.y
                });
            }
            else
            {
                state.AddRange(new List<float>(){
                    -1f, 0f, 0f, 0f, 0f
                });
            }

            for (int i = 0; i < kross.animator.layerCount; i++)
            {
                AnimatorStateInfo stateInfo = kross.animator.GetCurrentAnimatorStateInfo(0);
                state.Add(BoolToPlusMinus(stateInfo.IsName("Base Layer.loadDrawShoot.arrowManInit")));
                state.Add(BoolToPlusMinus(stateInfo.IsName("Base Layer.loadDrawShoot.Draw")));
                state.Add(BoolToPlusMinus(stateInfo.tagHash == kross.endAnimatorTagHash));
            }

            state.Add(kross.lastDraw.x);
            state.Add(kross.lastDraw.y);
        }
        else
        {
            state = new List<float>(new float[22]);
        }

        return state;
    }

    private float BoolToPlusMinus(bool bool_)
    {
        return bool_ ? 1f : -1f;
    }

    private List<float> IntToOneHotVector(int current_i, int length)
    {
        List<float> oneHotVector = new List<float>();
        for (int i = 0; i < length; i++)
        {
            oneHotVector.Add(i == current_i ? 1f : 0f);
        }
        return oneHotVector;
    }

    public void HardAttackReward()
    {
        reward += 0.02f;
    }

    public void WeakAttackReward()
    {
        reward += 0.01f;
    }

    public void ShootReward(string drawStr)
    {
        switch (drawStr)
        {
            case "0":
                reward += 0.005f;
                break;
            case "1":
                reward += 0.01f;
                break;
            case "2":
                reward += 0.02f;
                break;
        }
    }

    public void DestructionReward(PlayerController myPc)
    {
        // 自分が破滅したときに呼ばれる
        reward -= 5f;
        if (myPc.lastHitPc != null && myPc.lastHitPc.agent != null)
        {
            if (myPc.lastHitPc == opponentHolder.jonyPc || myPc.lastHitPc == opponentHolder.krossPc)
            {
                myPc.lastHitPc.agent.reward += 5f;
            }
        }
    }

    public void TakeHitReward(float damage, float impactMagnitude)
    {
        // 自分が攻撃受けたときによばれる
        float additionalReward = 0.5f * damage / 100f + 0.5f * impactMagnitude / 20f;
        reward -= additionalReward;
        if (opponentAgentController != null)
        {
            opponentAgentController.reward += additionalReward;
        }

    }

}
