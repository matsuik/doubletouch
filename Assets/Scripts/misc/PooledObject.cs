﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObject : MonoBehaviour {
	public float activeDuration = 2.0f;

	// Use this for initialization
	void OnEnable () {
		Invoke ("DisableMyself", activeDuration);
	}
	
	void DisableMyself(){
		gameObject.SetActive (false);
	}

	void OnDisable (){
		CancelInvoke ();
	}
}
