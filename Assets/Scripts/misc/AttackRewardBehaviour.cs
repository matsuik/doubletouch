﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRewardBehaviour : StateMachineBehaviour
{
    protected AgentsController agent;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (agent == null)
        {
            agent = animator.gameObject.GetComponent<PlayerController>().agent;
        }

        if (agent != null)
        {
            SpecificReward();
        }
    }

    virtual public void SpecificReward()
    {

    }
}
