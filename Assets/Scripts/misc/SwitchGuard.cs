﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchGuard : StateMachineBehaviour
{	
	private int? guardParamHash = null;
	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (guardParamHash == null) {
			guardParamHash = animator.HashAnimatorParam ("guard");
		}	
		animator.SetBool ((int)guardParamHash, false);
	}


}
