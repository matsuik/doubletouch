﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : Photon.PunBehaviour, IPunObservable
{
    Rigidbody2D rb2d;
    public Vector2 velocity;

    private ContactFilter2D contactFilter;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[4];
    private List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(4);
    private bool isOnSurface;

    private bool isOperating;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        contactFilter.useTriggers = true;
        contactFilter.SetLayerMask(LayerMask.GetMask("Level"));
        contactFilter.useLayerMask = true;

        if (PhotonNetwork.connected)
        {
            isOperating = photonView.isMine;
        }
        else
        {
            isOperating = true;
        }

    }

    void OnEnable()
    {
        isOnSurface = false;
    }


    void FixedUpdate()
    {
        if (isOperating && !isOnSurface)
        {
            velocity += Physics2D.gravity * Time.deltaTime;

            Vector2 deltaPosition = velocity * Time.deltaTime;
            Movement(deltaPosition);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (isOperating && !isOnSurface)
        {
            transform.rotation = Quaternion.FromToRotation(Vector3.right, velocity.normalized);
        }
    }

    void Movement(Vector2 deltaPosition)
    {
        float magnitudeDeltaPosition = deltaPosition.magnitude;

        int castHitCount = rb2d.Cast(deltaPosition, contactFilter, hitBuffer, magnitudeDeltaPosition);
        hitBufferList.Clear();
        for (int i = 0; i < castHitCount; i++)
        {
            hitBufferList.Add(hitBuffer[i]);
        }

        // このforで当たったcolliderまでの最小距離を求めてる
        for (int i = 0; i < hitBufferList.Count; i++)
        {
            if (i == 0)
            {
                isOnSurface = true;
                velocity = velocity * 0.001f;
            }
            StartCoroutine(DestroyCoroutine());
            // deltaPositoinのmagnitudeをcolliderまでの距離に置き換える
            float distanceToCollider = hitBufferList[i].distance;
            // の最小値を求めてる
            magnitudeDeltaPosition = distanceToCollider < magnitudeDeltaPosition ? distanceToCollider : magnitudeDeltaPosition;
        }

        rb2d.position = rb2d.position + deltaPosition.normalized * magnitudeDeltaPosition; // これやと衝突するポイントで一瞬とまる
    }

    void OnDisable()
    {
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    private IEnumerator DestroyCoroutine()
    {
        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(0.5f));
        gameObject.SetActive(false);
    }


    #region IPunObservable implementation
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // velocityがないと位置かえられへんから
        if (stream.isWriting)
        {
            stream.SendNext(velocity);
        }
        else
        {
            velocity = (Vector2)stream.ReceiveNext();
        }

    }
    #endregion
}

