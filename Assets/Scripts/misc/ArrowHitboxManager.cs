﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowHitboxManager : HitboxManager
{
    public float impactMultiplier = 0.1f;
    private ArrowController arrowController;
    protected override void Awake()
    {
        base.Awake();
        arrowController = GetComponent<ArrowController>();
    }

    protected override void Start()
    {
        if (PhotonNetwork.connected)
        {
            if (myPlayerController == null) // mineのはKrossのscriptから与えてもらえてる
            {
                var arrayKross = FindObjectsOfType<Kross>();
                foreach (var kross in arrayKross)
                {
                    if (!kross.photonView.isMine)
                    {
                        myPlayerController = (PlayerController)kross;
                        break;
                    }
                }
            }

            if (myPlayerController == null)
            {
                Debug.LogError("myPlayerController == null");
            }
        }
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject != myPlayerController.gameObject)
        {
            impactVelocity = impactMultiplier * arrowController.velocity;
            base.OnTriggerEnter2D(other);
        }
    }
}