﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxManager : MonoBehaviour
{
    public float damage;
    public Vector2 impactDirection;
    public float impactMagnitude;
    public float impactCoef;
    // 累積ダメージが100になったときに初期値の何倍が足されるか

    public float damageEffectPower;
    public float hitstun;
    public float hitstunCoef;
    // 累積ダメージが100になったときに初期値の何倍が足されるか

    public float attackLag;

    public float constantDuration;

    public float weakenCoef = 1f;
    // Animationから簡単に弱いヒットボックスを作れるようにするための変数

    private float decayRate = 25f;

    public bool doFlip = true;
    protected Vector2 impactVelocity;
    private List<int> colliderIdList = new List<int>(16);

    [HideInInspector]
    public PlayerController myPlayerController;

    protected virtual void Awake()
    {
        impactVelocity = impactDirection.normalized * impactMagnitude;
        if (gameObject.transform.parent != null)
        {
            myPlayerController = gameObject.transform.parent.GetComponent<PlayerController>();
        }

    }

    protected virtual void Start()
    {
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {

        if (!colliderIdList.Contains(other.GetInstanceID()) && other.CompareTag("Player"))
        {

            bool to_call = true;
            if (PhotonNetwork.connected)
            {
                if (NetworkGameController.gameController.friendlyFire)
                {
                    to_call = true;
                }
                else
                {
                    to_call = myPlayerController.photonView.isMine != other.gameObject.GetComponent<PlayerController>().photonView.isMine;
                }
            }
            else
            {
                if (AIGameController.gameController.friendlyFire)
                {
                    to_call = true;
                }
                else
                {
                    to_call = !myPlayerController.myHolder.goList.Contains(other.gameObject);
                }
            }

            if (to_call)
            {

                colliderIdList.Add(other.GetInstanceID());

                PlayerController otherPlayerController = other.gameObject.GetComponent<PlayerController>();
                if (otherPlayerController.isOperating)
                {
                    Debug.Log(impactVelocity);

                    Vector2 flipedImpactVelocity;
                    if (!doFlip || gameObject.transform.parent.rotation.eulerAngles.y == 0f)
                    {
                        flipedImpactVelocity = impactVelocity;
                    }
                    else
                    {
                        flipedImpactVelocity = new Vector2(-impactVelocity.x, impactVelocity.y);
                    }
                    otherPlayerController.TakeHit(new Impact(damage, impactCoef, hitstun, hitstunCoef,
                                                             attackLag, constantDuration, decayRate,
                                                             flipedImpactVelocity, damageEffectPower, weakenCoef),
                                                             myPlayerController);


                }
            }
        }
    }

    void OnDisable()
    {
        colliderIdList.Clear();
    }

}

public class Impact
{
    public float damage;

    private float impactCoef;
    // 累積ダメージが100になったときに初期値の何倍が足されるか
    public float hitstun;
    private float hitstunCoef;
    // 累積ダメージが100になったときに初期値の何倍が足されるか

    public float attackLag;

    public Vector2 impactVelocity;

    private float constantDuration;

    private float groundDecayRate;

    public float startTime;


    private bool isFirstGrounded = true;

    private float heaviness;

    private float aerialDecayRate;

    private float damageEffectPower;

    public Impact()
    {
        startTime = 0f;
        impactVelocity = Vector2.zero;
        constantDuration = 0f;
    }

    public Impact(float damage, float impactCoef, float hitstun, float hitstunCoef, float attackLag,
                 float constantDuration, float decayRate, Vector2 impactVelocity, float damageEffectPower,
                float weakenCoef)
    {
        weakenCoef = Mathf.Clamp(weakenCoef, 0f, 1f);
        this.damage = damage * weakenCoef;
        this.impactCoef = impactCoef;
        this.hitstun = hitstun * weakenCoef;
        this.hitstunCoef = hitstunCoef;
        this.attackLag = attackLag * weakenCoef;
        this.impactVelocity = impactVelocity * weakenCoef;
        this.constantDuration = constantDuration;
        this.groundDecayRate = decayRate;
        this.damageEffectPower = damageEffectPower;
    }

    public void ComputeDamageEffect(float guardMultiplier, float heaviness, float cumulativeDamage)
    {
        this.heaviness = heaviness;
        impactVelocity = (1f + impactCoef * Mathf.Pow(cumulativeDamage / 100f, damageEffectPower)) * impactVelocity * guardMultiplier / heaviness;
        aerialDecayRate = groundDecayRate / Mathf.Pow(impactVelocity.x, 2f);
        hitstun = (1f + hitstunCoef * Mathf.Pow(cumulativeDamage / 100f, damageEffectPower)) * hitstun * guardMultiplier;
        attackLag = Mathf.Clamp((1f + hitstunCoef * Mathf.Pow(cumulativeDamage / 100f, damageEffectPower)) * attackLag, 0f, 3f * attackLag);
    }

    public void ComputeDI(float DIsensitivity, Vector2 lastTouchDeltaPosition)
    {
        Vector2 perpendicular = new Vector2(-impactVelocity.y, impactVelocity.x);
        Vector2 projected = DIsensitivity * Vector2.Dot(perpendicular.normalized, lastTouchDeltaPosition.normalized) * perpendicular;
        impactVelocity = impactVelocity.magnitude * (impactVelocity + projected) / (impactVelocity + projected).magnitude;
    }

    public void Decay(bool grounded, float deltaTime)
    {
        if (impactVelocity.x != 0f)
        {
            if (grounded)
            {
                if (impactVelocity.x > 0)
                {
                    impactVelocity.x = Mathf.Clamp(impactVelocity.x - heaviness * groundDecayRate * deltaTime, 0f, impactVelocity.x);
                }
                else
                {
                    impactVelocity.x = Mathf.Clamp(impactVelocity.x + heaviness * groundDecayRate * deltaTime, impactVelocity.x, 0f);
                }
            }
            else
            {
                if (impactVelocity.x > 0)
                {
                    impactVelocity.x = Mathf.Clamp(impactVelocity.x - aerialDecayRate * Mathf.Pow(impactVelocity.x, 2f) * deltaTime, 0f, impactVelocity.x);
                }
                else
                {
                    impactVelocity.x = Mathf.Clamp(impactVelocity.x + aerialDecayRate * Mathf.Pow(impactVelocity.x, 2f) * deltaTime, impactVelocity.x, 0f);
                }
            }
        }
    }

    public bool IsConst(float now)
    {
        return (now - startTime) < constantDuration;
    }

    public void Grounded()
    {
        if (isFirstGrounded)
        {
            if (Mathf.Abs(impactVelocity.x) < 1f)
            {
                impactVelocity.x = 0f;
            }
            impactVelocity.y = 0f;
            isFirstGrounded = false;
        }
    }

    public void Jump()
    {
        impactVelocity.x *= 0.5f;
    }

}
