﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpSquatBehaviour : StateMachineBehaviour
{
    private float squatDuartion;

    private PlayerController playerController;

    private float startTime;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (playerController == null)
        {
            playerController = animator.gameObject.GetComponent<PlayerController>();
            squatDuartion = 0.05f / stateInfo.speed; // 3フレーム=51ms
        }
        startTime = Time.time;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (squatDuartion < (Time.time - startTime))
        {
            playerController.isJumpingFromSquat = true;
        }
    }

}
