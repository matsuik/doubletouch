﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Photon.PunBehaviour, ITouchable
{
    public bool isLogEnabled;

    // vary over characters
    public float heaviness = 1f;

    public float inputGroundSpeed = 7f;
    public float inputAerialAccel = 3f;

    public float jumpVerticalSpeed = 7f;
    public float jumpHorizontalSpeed = 2f;

    public float maxAerialHorizontalSpeed = 3f;

    public float originalGravityModifier = 1f;

    public float fastFallGravityMultiplier = 3f;

    public float ledgeOffSpeedMultiplier = 0.5f;

    public float DIsensitivity = 0.3f;

    // const over characters
    // PYSICS
    private Rigidbody2D rb2d;

    [HideInInspector]
    public Vector2 velocity;
    private float horizontalAccel;

    private float currentGravityModifier;

    private ContactFilter2D contactFilter;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    private List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    public bool grounded { get; private set; }
    // false: every FixedUpdate
    // true: when grounded Movement in FixedUpdate

    private Vector2 groundNormal;

    private const float minGroundNormalY = .65f;
    private const float minMoveDistance = 0.001f;
    private const float shellRadius = 0.01f;

    // INPUT
    private Vector2 inputVector;

    [HideInInspector]
    public Vector2 animationMoveVector;
    // directly modified by animation

    public float touchHorizontalThreshold = 2;
    public const float touchJumpHorizontalThreshold = 20;
    public const float touchJumpVerticalThreshold = 20;

    public float touchGuardThreshold = 10;

    protected float touchStillStartTime;

    public bool isTouched { get; private set; }
    // AIに勝手にbeginしてもらうために必要

    private bool startingJumpSquat;

    [HideInInspector]
    public bool isJumpingFromSquat;
    private bool oldGrounded;
    private bool doDoubleJump;
    // true: when doubleJump OnTouchMoved
    // false : after doubleJump Update
    private float doubleJumpDelayDuration = 0.3f;

    public int numPossibleDoubleJump { get; private set; }

    public bool delayingDoubleJump { get; private set; }
    // <belief>: only used to delay another double jump after jump

    public bool hasFastFallen { get; private set; }
    // false: when grounded Movement in FixedUpdate, after doubleJump OnTouchMoved
    // true: after fastFall

    public bool canMove { get; private set; }
    // false : when get hit HitStun
    // true : after hitstun Hitstun
    private bool duringHitLag;

    private bool duringAttackLag;

    [HideInInspector]
    public bool isAttacking;
    // <belief>: should be modified from animations. groundedのときはひっくり返さない。animationMoveVectorを有効にする。空中ではあんま何もしてない

    private BoxCollider2D mainCollider;
    private Vector2 defaultMainColliderOffset;
    private Vector2 residualMainColliderOffset;

    [HideInInspector]
    public bool isColliderMovedByAnimation;

    private Collider2D[] overlapLevelCollder;

    private bool hasTakenHit;
    // true: when get hit TakeHit
    // false: every LastUpdate

    [HideInInspector]
    public PlayerController lastHitPc;

    public float cumulativeDamage { get; private set; }

    [HideInInspector]
    public Impact currentImpact { get; private set; }

    [HideInInspector]
    public Animator animator;
    private readonly int hitstunAnimatorParamHash = Animator.StringToHash("hitstun");
    private readonly int groundedAnimatorParamHash = Animator.StringToHash("grounded");
    private readonly int horizontal_speedAnimatorParamHash = Animator.StringToHash("horizontal_speed");
    private readonly int vertical_speedAnimatorParamHash = Animator.StringToHash("vertical_speed");

    public int guardAnimatorParamHash { get; private set; }
    // constはコンパイル時に与えないとあかんから、使えないよ

    private int jumpSquatAnimatorParamHash;

    [HideInInspector]
    public AgentsController agent;

    public bool isOperating { get; private set; }
    // <brief> : 物理とかTakeHitを動かすか

    private bool canTouch;

    [HideInInspector]
    public PlayerHolder myHolder;

    public bool changeColor = true;

    private Vector2 lastTouchDeltaPosition;

    [HideInInspector]
    public bool isFinished;

    // [HideInInspector]
    public PlayerUI playerUI;

    #region MonoBehaviour

    // Use this for initialization
    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
        mainCollider = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        guardAnimatorParamHash = animator.HashAnimatorParam("guard");
        jumpSquatAnimatorParamHash = animator.HashAnimatorParam("jump_squat");
    }

    protected virtual void Start()
    {

        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;


        defaultMainColliderOffset = mainCollider.offset;

        if (PhotonNetwork.connected)
        {
            if (!photonView.isMine)
            //自分のじゃなかったら色変える
            {
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.5f, 0.5f);
            }
            // 繋がってて自分のなら動かす
            isOperating = photonView.isMine;

            canTouch = photonView.isMine;
        }
        else
        {
            if (transform.parent != null)
            {
                agent = transform.parent.gameObject.GetComponent<AgentsController>();
            }

            if (agent != null && changeColor)
            {
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.5f, 0.5f);
            }

            // 繋がってないときはAIでもPersonでも動かす
            isOperating = true;

            // agent が ついてなければ personなのでTouchできる
            canTouch = (agent == null);

            myHolder = transform.parent.gameObject.GetComponent<PlayerHolder>();
            if (myHolder == null)
            {
                Debug.LogError("PlayerController should be holded by a parent PlayerHolder");
            }
        }

        InitPlayerUI();

        Initialize();
    }

    private void InitPlayerUI()
    {
        var playerUIArray = (PlayerUI[])FindObjectsOfType(typeof(PlayerUI));
        if (isLogEnabled) Debug.Log($"playerUIArray.Length: {playerUIArray.Length}");
        foreach (var playerUIfound in playerUIArray)
        {
            if (isLogEnabled) Debug.Log($"playerUIfound.playerController: {playerUIfound.playerController}");

            if (playerUIfound.playerController == null)
            {
                playerUIfound.playerController = this;
                playerUI = playerUIfound;
                break;
            }
        }

        if (playerUI != null)
        {
            playerUI.SetSprite(GetComponent<SpriteRenderer>());
        }
        else
        {
            Debug.LogError("No empty player UI");
        }
    }

    /// StartとAgentResetで呼ぶ, 交代するときにはダメージはそのままでimpactも保存するので呼ばない
    public void Initialize()
    {
        cumulativeDamage = 0f;
        currentImpact = new Impact();
        isFinished = false;
        GeneralInitialize();
    }

    public void Transit(Transform newTransorm, Impact newImpact)
    {
        transform.position = newTransorm.position;
        transform.rotation = newTransorm.rotation;
        currentImpact = newImpact;
        GeneralInitialize();
        gameObject.SetActive(true);
    }


    void GeneralInitialize()
    {
        velocity = Vector2.zero;
        horizontalAccel = 0f;
        currentGravityModifier = originalGravityModifier;
        inputVector = Vector2.zero;
        animationMoveVector = Vector2.zero;
        canMove = true;
        numPossibleDoubleJump = 2;
        doDoubleJump = false;
        hasTakenHit = false;
        hasFastFallen = false;
        isAttacking = false;
        isColliderMovedByAnimation = false;
        delayingDoubleJump = false;
        duringHitLag = false;
        duringAttackLag = false;
        startingJumpSquat = false;
        isJumpingFromSquat = false;
        oldGrounded = false;
        isTouched = false;
        lastHitPc = null;
        overlapLevelCollder = new Collider2D[1];
        residualMainColliderOffset = Vector2.zero;
        grounded = false;
        hitBufferList.Clear();
        lastTouchDeltaPosition = Vector2.zero;

        SpecificInitialize();
    }

    protected virtual void SpecificInitialize()
    {
    }

    void FixedUpdate()
    {
        if (isOperating)
        {
            if (canMove)
            {
                bool isLookingRignt = (transform.rotation.eulerAngles.y == 0f);

                // hitstunでひっくり返したあとにもinputGoundVelocityは残ってるので, そのあとひっくり返さないようにする
                if (grounded && !isAttacking)
                {
                    if (isLookingRignt && inputVector.x < 0f)
                    { // 右向いてて, 左に進むので, 左にひっくり返す
                        transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
                    }
                    else if (!isLookingRignt && inputVector.x > 0f)
                    { // 左向いてて, 右に進むので、右にひっくり返す
                        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                    }
                }

                animationMoveVector.x = isLookingRignt ? animationMoveVector.x : -animationMoveVector.x;

                if (grounded)
                {
                    if (isJumpingFromSquat)
                    {
                        StartCoroutine(DelayDoubleJumpCoroutine());
                        currentImpact.Jump();
                        inputVector.y = jumpVerticalSpeed;

                        if (lastTouchDeltaPosition.x > touchJumpHorizontalThreshold)
                        {
                            inputVector.x = jumpHorizontalSpeed;
                        }
                        else if (lastTouchDeltaPosition.x < -touchJumpHorizontalThreshold)
                        {
                            inputVector.x = -jumpHorizontalSpeed;
                        }
                        else
                        {
                            inputVector.x = 0f;
                        }
                        velocity = inputVector;
                        horizontalAccel = 0f;
                    }
                    else
                    {
                        velocity = isAttacking ? animationMoveVector : inputVector;
                        horizontalAccel = 0f;
                        if (startingJumpSquat)
                        {
                            StartCoroutine(PulseAnimatorParamBoolCoroutine(jumpSquatAnimatorParamHash));

                        }
                    }
                }
                else
                {
                    if (!isAttacking && doDoubleJump)
                    { // doDoubleJumpは 普通の空中状態での慣性移動と区別するために必要
                        velocity = inputVector;
                        horizontalAccel = 0f;
                        numPossibleDoubleJump -= 1;
                        StartCoroutine(DelayDoubleJumpCoroutine());
                        currentImpact.Jump();
                    }
                    else
                    {
                        velocity += animationMoveVector;
                        if (!startingJumpSquat && oldGrounded && !grounded)
                        { // 崖から降りるとき
                            velocity.x = velocity.x * ledgeOffSpeedMultiplier;
                        }
                        horizontalAccel = inputVector.x;
                    }
                }

            } // end if (canMove)

            if (!duringHitLag && !duringAttackLag)
            {
                if (!currentImpact.IsConst(Time.time))
                {
                    velocity += currentGravityModifier * Physics2D.gravity * Time.deltaTime;
                }

                if (!grounded)
                {
                    velocity.x = Mathf.Clamp(velocity.x + horizontalAccel * Time.deltaTime, -maxAerialHorizontalSpeed, maxAerialHorizontalSpeed);
                }

                grounded = false;

                Vector2 deltaPosition = (velocity + new Vector2(currentImpact.impactVelocity.x, 0f)) * Time.deltaTime;

                Vector2 vectorAlongGround = new Vector2(groundNormal.y, -groundNormal.x);
                Vector2 deltaPositionAlongGround = vectorAlongGround * deltaPosition.x; // slopeの方向にxだけ動く, x軸を向いてるとは限らない
                                                                                        // slopeが上向いてても同じ速度ですすむから、現実で坂を登るときみたいにならへん, 射影したらまだましかも

                Movement(deltaPositionAlongGround, false);

                Vector2 ymove = Vector2.up * deltaPosition.y;

                Movement(ymove, true);

                if (grounded)
                {
                    lastHitPc = null;
                }

                currentImpact.Decay(grounded, Time.deltaTime);

            }

            animator.SetBool(groundedAnimatorParamHash, grounded);
            animator.SetFloat(horizontal_speedAnimatorParamHash, Mathf.Abs(velocity.x) / inputGroundSpeed);
            animator.SetFloat(vertical_speedAnimatorParamHash, velocity.y);

            inputVector = Vector2.zero;
            oldGrounded = grounded;
            doDoubleJump = false;
            startingJumpSquat = false;
            isJumpingFromSquat = false;
        } // end if(isOperating)
    } // end void FixedUpdate()



    void OnAnimatorMove()
    {
        if (isOperating)
        {
            resolveOverlap();
        }
    }

    void LastUpdate()
    {
        if (isOperating)
        {
            // TODO: animationでcollider.offsetいじるとpysics使ってくれないので変えるべき
            // 攻撃中にhitされたら、colliderのoffset分移動する
            if (hasTakenHit)
            {
                hasTakenHit = false;
                rb2d.position += new Vector2(residualMainColliderOffset.x, residualMainColliderOffset.y);
                residualMainColliderOffset = Vector2.zero;
            }
        }

    }

    #endregion

    // in FixedUpdate
    void Movement(Vector2 deltaPosition, bool yMovement)
    {
        float magnitudeDeltaPosition = deltaPosition.magnitude;

        // このチェックは効率いいけど、animationによるcolliderの移動で壁にあたると浮く仕様で、そのあと静止からの落下になって遅くて, groundedとかがおかしくなる
        if (magnitudeDeltaPosition > minMoveDistance)
        { //床についてるときは毎回velocityがresetされて小さい値になってる。 落ち始めも小さいけどまあチェックせんでいいかな
            int castHitCount = rb2d.Cast(deltaPosition, contactFilter, hitBuffer, magnitudeDeltaPosition + shellRadius);
            hitBufferList.Clear();
            for (int i = 0; i < castHitCount; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            // このforで当たったcolliderまでの最小距離を求めてる
            for (int i = 0; i < hitBufferList.Count; i++)
            {
                Vector2 currentNormal = hitBufferList[i].normal;

                // slopeがある傾き以下なら、地面についてることにする, 
                if (currentNormal.y > minGroundNormalY)
                {
                    if (!grounded)
                    {
                        grounded = true;
                        numPossibleDoubleJump = 2;
                        hasFastFallen = false;
                        currentGravityModifier = originalGravityModifier;
                        currentImpact.Grounded();
                    }

                    if (yMovement)
                    {
                        groundNormal = currentNormal; // groundNormalは今立ってるところのNormal yMovementじゃないと検出できへん？
                        currentNormal.x = 0; // スロープの有る地面も垂直上を向いてるとみなす, けどこの後のprojectionの計算では弱くなるよね
                    }
                }

                // currentNormal方向のvelocityだけ打ち消す 壁に当たったときとか
                // float projection = Vector2.Dot(velocity, currentNormal);
                // if (projection < 0)
                // {
                //     velocity = velocity - projection * currentNormal;
                // }

                // deltaPositoinのmagnitudeをcolliderまでの距離に置き換える
                float distanceToCollider = hitBufferList[i].distance - shellRadius;
                // の最小値を求めてる
                magnitudeDeltaPosition = distanceToCollider < magnitudeDeltaPosition ? distanceToCollider : magnitudeDeltaPosition;

            }
        }

        rb2d.position = rb2d.position + deltaPosition.normalized * magnitudeDeltaPosition; // これやと衝突するポイントで一瞬とまる

    }


    // called by HitboxManager: OnTriggerEnter
    public void TakeHit(Impact impact, PlayerController takeHitPc)
    {
        currentImpact = impact;
        float guardMultiplier = animator.GetBool(guardAnimatorParamHash) ? 0.5f : 1f;
        cumulativeDamage += currentImpact.damage;
        playerUI.UpdateDamage(cumulativeDamage);
        currentImpact.ComputeDamageEffect(guardMultiplier, heaviness, cumulativeDamage);
        if (currentImpact.attackLag > 0)
        {
            takeHitPc.CallAttackLag(currentImpact.attackLag);
        }
        velocity = Vector2.zero;
        CallHitLag(takeHitPc);

        if (transform.rotation.eulerAngles.y == 0f && currentImpact.impactVelocity.x > 0f)
        { // 右向いてるときに、右ににふっとばされたら, 左にひっくり返す
            transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
        }
        else if (transform.rotation.eulerAngles.y == 180f && currentImpact.impactVelocity.x < 0f)
        { // 左向いてるときに、左ににふっとばされたら、右にひっくり返す
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        }

        if (numPossibleDoubleJump == 0)
        {
            numPossibleDoubleJump = 1;
        }

        hasTakenHit = true;
        residualMainColliderOffset = mainCollider.offset - defaultMainColliderOffset;

        if (agent != null)
        {
            agent.TakeHitReward(currentImpact.damage, currentImpact.impactVelocity.magnitude);
        }
    }

    private IEnumerator currentHitLagCoroutine;

    private void CallHitLag(PlayerController takeHitPc)
    {
        if (currentHitStunCoroutine != null)
        {
            StopCoroutine(currentHitStunCoroutine);
        }

        if (currentHitLagCoroutine == null)
        {
            currentHitLagCoroutine = HitLagCancelableCoroutine(takeHitPc);
            StartCoroutine(currentHitLagCoroutine);
        }
        else
        {
            StopCoroutine(currentHitLagCoroutine);
            currentHitLagCoroutine = HitLagCancelableCoroutine(takeHitPc);
            StartCoroutine(currentHitLagCoroutine);
        }
    }


    // must be called by callHitLag
    private IEnumerator HitLagCancelableCoroutine(PlayerController takeHitPc)
    {
        canMove = false;
        duringHitLag = true;
        animator.SetBool(hitstunAnimatorParamHash, true);

        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(currentImpact.attackLag));

        duringHitLag = false;
        currentImpact.ComputeDI(DIsensitivity, lastTouchDeltaPosition);
        velocity.y = currentImpact.impactVelocity.y;
        currentImpact.startTime = Time.time;
        lastHitPc = takeHitPc;
        CallHitStun();
    }

    private IEnumerator currentHitStunCoroutine;

    private void CallHitStun()
    {
        if (currentHitStunCoroutine == null)
        {
            currentHitStunCoroutine = HitStunCancelableCoroutine();
            StartCoroutine(currentHitStunCoroutine);
        }
        else
        {
            StopCoroutine(currentHitStunCoroutine);
            currentHitStunCoroutine = HitStunCancelableCoroutine();
            StartCoroutine(currentHitStunCoroutine);
        }
    }


    // must be called by callHitStun
    private IEnumerator HitStunCancelableCoroutine()
    {
        canMove = false;
        animator.SetBool(hitstunAnimatorParamHash, true);

        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(currentImpact.hitstun));

        canMove = true;
        animator.SetBool(hitstunAnimatorParamHash, false);
    }

    private IEnumerator currentAttackLagCoroutine;
    private float originalSpeed;

    public void CallAttackLag(float attackLagDuration)
    {
        if (currentAttackLagCoroutine == null)
        {
            originalSpeed = animator.speed;
            currentAttackLagCoroutine = AttackLagCancelableCoroutine(attackLagDuration, originalSpeed);
            StartCoroutine(currentAttackLagCoroutine);
        }
        else
        {
            StopCoroutine(currentAttackLagCoroutine);
            currentAttackLagCoroutine = AttackLagCancelableCoroutine(attackLagDuration, originalSpeed);
            StartCoroutine(currentAttackLagCoroutine);
        }
    }


    // must be called by callAttackLag
    private IEnumerator AttackLagCancelableCoroutine(float attackLagDuation, float originalSpeed)
    {
        animator.speed = 0f;
        duringAttackLag = true;

        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(attackLagDuation));

        animator.speed = originalSpeed;
        duringAttackLag = false;
    }


    protected bool ProcessStandardTouchMoved(Touch touch)
    {
        // [ground, not jump], [ground, do jump], [not ground, not jump], [not ground, do jump]
        //Debug.Log("touch.deltaPosition" + touch.deltaPosition);
        if (canMove &&
            (touch.deltaPosition.x > touchHorizontalThreshold || touch.deltaPosition.x < -touchHorizontalThreshold
            || touch.deltaPosition.y > touchJumpVerticalThreshold || touch.deltaPosition.y < -touchJumpVerticalThreshold)
            )
        { // touche moved
            touchStillStartTime = Time.time;

            if (animator.GetBool(guardAnimatorParamHash))
            {
                if (Mathf.Abs(touch.deltaPosition.x) > touchGuardThreshold || touch.deltaPosition.y > touchJumpVerticalThreshold)
                {
                    animator.SetBool(guardAnimatorParamHash, false);
                }
            }
            else if (grounded)
            {

                //[ground, do jump] verticalとhorizontalがある
                if (touch.deltaPosition.y > touchJumpVerticalThreshold)
                {
                    startingJumpSquat = true;
                }
                else if (touch.deltaPosition.y < -touchGuardThreshold)
                {
                    animator.SetBool(guardAnimatorParamHash, true);

                }
                else
                { // [ground, not jump]
                    inputVector.y = 0f;

                    if (touch.deltaPosition.x > touchHorizontalThreshold)
                    {
                        inputVector.x = inputGroundSpeed;
                    }
                    else if (touch.deltaPosition.x < -touchHorizontalThreshold)
                    {
                        inputVector.x = -inputGroundSpeed;
                    }
                    else
                    {
                        inputVector = Vector2.zero;
                    }
                }

                // not grounded
            }
            else
            {
                // [not grounded, do jump]
                if ((numPossibleDoubleJump > 0) && !delayingDoubleJump && touch.deltaPosition.y > touchJumpVerticalThreshold)
                {
                    doDoubleJump = true; // doDoubleJumpは 普通の空中状態での慣性移動と区別するために必要
                    hasFastFallen = false;
                    currentGravityModifier = originalGravityModifier;

                    inputVector.y = jumpVerticalSpeed;

                    if (touch.deltaPosition.x > touchJumpHorizontalThreshold)
                    {
                        inputVector.x = jumpHorizontalSpeed;
                    }
                    else if (touch.deltaPosition.x < -touchJumpHorizontalThreshold)
                    {
                        inputVector.x = -jumpHorizontalSpeed;
                    }
                    else
                    {
                        inputVector.x = 0f;
                    }


                }
                else
                { // [not gronded, not jump]
                    if (touch.deltaPosition.x > touchHorizontalThreshold)
                    {
                        inputVector.x = inputAerialAccel;
                    }
                    else if (touch.deltaPosition.x < -touchHorizontalThreshold)
                    {
                        inputVector.x = -inputAerialAccel;
                    }
                    else
                    {
                        inputVector.x = 0f;
                    }

                    if ((!hasFastFallen) && touch.deltaPosition.y < -touchJumpVerticalThreshold)
                    {
                        hasFastFallen = true;
                        currentGravityModifier = fastFallGravityMultiplier * originalGravityModifier;
                    }
                } // end [not grounded, do jump] or [not gronded, not jump]
            } // end grounded or not
            return true; // isMoved
        }  // end touche moved
        else
        {
            return false; // isMoved is false
        }
    }

    private IEnumerator DelayDoubleJumpCoroutine()
    {
        delayingDoubleJump = true;

        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(doubleJumpDelayDuration));

        delayingDoubleJump = false;
    }


    protected IEnumerator PulseAnimatorParamBoolCoroutine(int animatorParamHash)
    {
        // attackをonにして、1frame後にoffにする
        animator.SetBool(animatorParamHash, true);

        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(0.1f));

        animator.SetBool(animatorParamHash, false);
    }

    void resolveOverlap()
    {
        mainCollider.OverlapCollider(contactFilter, overlapLevelCollder);
        Collider2D otherCollider = overlapLevelCollder[0];
        if (otherCollider == null)
        {
            return;
        }

        ColliderDistance2D colliderDistance = mainCollider.Distance(otherCollider);
        if (colliderDistance.distance < -0.01)
        {
            rb2d.position = rb2d.position + colliderDistance.distance * colliderDistance.normal;
        }

        overlapLevelCollder[0] = null;
    }

    public void OnTouchBegan(Touch touch)
    {
        touchStillStartTime = Time.time;
        isTouched = true;
        lastTouchDeltaPosition = touch.deltaPosition;
        SpecificOnTouchBegan(touch);
    }

    public virtual void SpecificOnTouchBegan(Touch touch)
    {
    }

    public void OnTouchMoved(Touch touch)
    {
        inputVector = Vector2.zero;
        lastTouchDeltaPosition = touch.deltaPosition;
        SpecificOnTouchMoved(touch);
    }

    public virtual void SpecificOnTouchMoved(Touch touch)
    {
    }

    public void OnTouchEnded(Touch touch)
    {
        inputVector = Vector2.zero;
        animator.SetBool(guardAnimatorParamHash, false);
        isTouched = false;
        lastTouchDeltaPosition = Vector2.zero;
        SpecificOnTouchEnded(touch);
    }

    public virtual void SpecificOnTouchEnded(Touch touch)
    {
    }

    public bool CanTouch()
    {
        return canTouch;
    }
}
