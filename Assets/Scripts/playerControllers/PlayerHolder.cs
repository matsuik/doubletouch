﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHolder : MonoBehaviour
{
    [HideInInspector]
    public GameObject jonyGo;

    [HideInInspector]
    public GameObject krossGo;

    [HideInInspector]
    public PlayerController jonyPc;

    [HideInInspector]
    public PlayerController krossPc;

    [HideInInspector]
    public List<GameObject> goList;

    void Awake()
    {
        jonyGo = transform.Find("Jony").gameObject;
        krossGo = transform.Find("Kross").gameObject;

        goList = new List<GameObject>() { jonyGo, krossGo };

        jonyPc = jonyGo.GetComponent<PlayerController>();
        krossPc = krossGo.GetComponent<PlayerController>();
    }
}
