﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jony : PlayerController
{
    public float longTouchThreshold = 0.2f;
    private readonly int weak_attackAnimatorParamHash = Animator.StringToHash("weak_attack");
    private readonly int hard_attackAnimatorParamHash = Animator.StringToHash("hard_attack");

    public override void SpecificOnTouchMoved(Touch touch)
    {
        ProcessStandardTouchMoved(touch);
    }

    public override void SpecificOnTouchEnded(Touch touch)
    {
        float touchDuration = Time.time - touchStillStartTime;
        //Debug.Log ("touchDuration" + touchDuration);
        if (touchDuration < longTouchThreshold)
        {
            StartCoroutine(PulseAnimatorParamBoolCoroutine(weak_attackAnimatorParamHash));
        }
        else
        {
            StartCoroutine(PulseAnimatorParamBoolCoroutine(hard_attackAnimatorParamHash));
        }
    }
}
