﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;

// public class Kross_obsolete: PlayerController{
// 	public float maxDrawPixelDeltaPosition; // should be const
// 	public float loadTouchDuationThreshold;
// 	public GameObject arrow;
// 	private Vector2 loadPixelPosition;
// 	private bool isFirstFrameDrawing;

// 	private readonly int loadAnimatorParamHash = Animator.StringToHash("load");
// 	private readonly int shootAnimatorParamHash = Animator.StringToHash("shoot");
// 	private readonly int vertical_drawAnimatorParamHash = Animator.StringToHash("vertical_draw");
// 	private readonly int horizontal_drawAnimatorParamHash = Animator.StringToHash("horizontal_draw");
// 	private readonly int drawAnimatorTagHash = Animator.StringToHash("draw");

// 	public Vector2[] directionArray = new Vector2[2];
// 	public float[] magnitudeArray = new float[4];

// 	private Dictionary<string, Vector2> directionDict;
// 	private Dictionary<string, float> magnitudeDict;

// 	private readonly Dictionary<string, int> transitionDict 
// 	= new Dictionary<string, int>()
// 	{	
// 		{"Ready", 1},
// 		{"0", 2},
// 		{"45", 3}
// 	};

// 	protected override void Awake ()
// 	{
// 		base.Awake ();

// 		directionDict = new Dictionary<string, Vector2>()
// 		{
// 			{"0", directionArray[0].normalized},
// 			{"45", directionArray[1].normalized}
// 		};


// 		magnitudeDict = new Dictionary<string, float>()
// 		{
// 			{"0", magnitudeArray[0]},
// 			{"1", magnitudeArray[1]},
// 			{"2", magnitudeArray[2]},
// 			{"3", magnitudeArray[3]}
// 		};

// 	}


// 	public override void OnTouchMoved(Touch touch){
// 		base.OnTouchMoved (touch);
// 		int tagHash = animator.GetCurrentAnimatorStateInfo (0).tagHash;
// 		if (tagHash == drawAnimatorTagHash) {
// 			DrawInputHandle (touch);
// 		} 
// 		else {
// 			bool isMoved = ProcessStandardTouchMoved (touch);

// 			if (!isMoved) {
// 				LoadArrow (touch);
// 			}
// 		}

// 	}

// 	private void DrawInputHandle(Touch touch){
// 		if (isFirstFrameDrawing) {
// 			loadPixelPosition = touch.position;
// 			isFirstFrameDrawing = false;
// 		}
// 		Vector2 draw = touch.position - loadPixelPosition;
// 		Debug.Log (draw);
// 		if (transform.rotation.eulerAngles.y == 0f) { // 右向いてる
// 			draw = -4f * draw / (maxDrawPixelDeltaPosition + 0.001f);
// 		} 
// 		else { // 左向いてるので左右逆にする
// 			draw = -4f * draw / (maxDrawPixelDeltaPosition + 0.001f);
// 			draw.x = -draw.x;
// 		}
// 		animator.SetFloat (horizontal_drawAnimatorParamHash, draw.x);
// 		animator.SetFloat (vertical_drawAnimatorParamHash, draw.y);
// 	}

// 	private void LoadArrow(Touch touch){
// 		float touchDuration = Time.time - touchStillStartTime;
// 		if (touchDuration > loadTouchDuationThreshold) {
// 			isFirstFrameDrawing = true;
// 			touchStillStartTime = Time.time;
// 			animator.SetBool (loadAnimatorParamHash, true);
// 		}
// 	}

// 	public override void OnTouchEnded (Touch touch)
// 	{	
// 		base.OnTouchEnded (touch);

// 		animator.SetBool (loadAnimatorParamHash, false);

// 		if (animator.GetCurrentAnimatorStateInfo (0).tagHash == drawAnimatorTagHash){
// 			ShootArrow();
// 		}
// 	}

// 	private void ShootArrow(){
// 		string clipName = animator.GetCurrentAnimatorClipInfo (0)[0].clip.name;
// 		string[] words = clipName.Split ('_'); // 0:arrow, 1:{Ready, Draw}, 2:{0, 45}, 3:{0, 1, 2, 3}
// 		if (words [1] == "Ready") {
// 			StartCoroutine (ShootTransition (transitionDict ["Ready"]));
// 			return;
// 		} else {
// 			Vector2 v0 = magnitudeDict [words [3]] * directionDict [words [2]];
// 			arrow.SetActive (true);
// 			if (transform.rotation.eulerAngles.y == 0f) { // 右向いてる
// 				arrow.GetComponent<Rigidbody2D> ().velocity = v0;
// 			} 
// 			else { // 左向いてるので左右逆にする
// 				v0.x = - v0.x;
// 				arrow.GetComponent<Rigidbody2D> ().velocity = v0;
// 			}

// 			StartCoroutine (ShootTransition (transitionDict [words [2]]));
// 		}
// 		Debug.Log (clipName);
// 	}

// 	private IEnumerator ShootTransition(int hashTransision){
// 		// attackをonにして、1frame後にoffにする
// 		animator.SetInteger(shootAnimatorParamHash, hashTransision);
// 		Debug.Log (hashTransision);

// 		yield return new WaitForSeconds(0.1f); // nullやとAnimatorが反応しないときもある

// 		animator.SetInteger(shootAnimatorParamHash, 0);
// 	}
// }
