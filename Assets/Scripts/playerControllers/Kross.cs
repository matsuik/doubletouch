﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Kross : PlayerController
{
    public bool isLoggingEnabled = false;
    public float maxDrawPixelDeltaPositionMagnitude; // should be const
    public float loadTouchDuationThreshold;
    private Vector2 loadPixelPosition;
    private bool isFirstFrameDrawing;

    private int loadAnimatorParamHash;
    private int shootAnimatorParamHash = Animator.StringToHash("shoot");
    private int vertical_drawAnimatorParamHash = Animator.StringToHash("vertical_draw");
    private int horizontal_drawAnimatorParamHash = Animator.StringToHash("horizontal_draw");
    private int drawAnimatorTagHash = Animator.StringToHash("draw");

    [HideInInspector]
    public int endAnimatorTagHash = Animator.StringToHash("end");



    public Vector2[] directionArray = new Vector2[2];
    public Transform[] transformArray = new Transform[2];
    public float[] magnitudeArray = new float[4];

    private Dictionary<string, Vector2> directionDict;
    private Dictionary<string, Transform> transformDict;
    private Dictionary<string, float> magnitudeDict;

    public GameObject arrowPrefab;

    [HideInInspector]
    public Vector2 lastDraw;

    [HideInInspector]
    public GameObject currentArrow;

    public float arrowSpawnDelay;

    private readonly Dictionary<string, int> transitionDict
    = MyUtils.Array2Dict<string, int>(new string[] { "Ready", "0", "45", "77", "-45", "-90" }, new int[] { 1, 2, 3, 4, 5, 6 });

    List<float> tmpBlendTreePositionList = new List<float>()
    {
        0.1f, 0f,
        0.2f, 0f,
        0.5f, 0f,
        1f, 0f,
        0.08f, 0.08f,
        0.14f, 0.14f,
        0.35f, 0.35f,
        0.7f, 0.7f,
        0f, 0.11f,
        0f, 0.2f,
        0f, 0.5f,
        0f, 1f,
        0.08f, -0.08f,
        0.14f, -0.14f,
        0.35f, -0.35f,
        0.7f, -0.7f,
        0f, -0.11f,
        0f, -0.2f,
        0f, -0.5f,
        0f, -1f
    };

    List<string> blendTreeClipNameList = new List<string>()
    {
        "arrow_Ready_0_0",
        "arrow_Draw_0_0",
        "arrow_Draw_0_1",
        "arrow_Draw_0_2",
        "arrow_Ready_45_0",
        "arrow_Draw_45_0",
        "arrow_Draw_45_1",
        "arrow_Draw_45_2",
        "arrow_Ready_77_0",
        "arrow_Draw_77_0",
        "arrow_Draw_77_1",
        "arrow_Draw_77_2",
        "arrow_Ready_-45_0",
        "arrow_Draw_-45_0",
        "arrow_Draw_-45_1",
        "arrow_Draw_-45_2",
        "arrow_Ready_-90_0",
        "arrow_Draw_-90_0",
        "arrow_Draw_-90_1",
        "arrow_Draw_-90_2",
    };

    List<Vector2> blendTreePositionList = new List<Vector2>() { };

    protected override void Awake()
    {
        base.Awake();
        loadAnimatorParamHash = animator.HashAnimatorParam("load");

        string[] shootAngles = { "0", "45", "77", "-45", "-90" };
        directionDict = MyUtils.Array2Dict<string, Vector2>(shootAngles, directionArray.Select(dir => dir.normalized).ToArray());
        transformDict = MyUtils.Array2Dict<string, Transform>(shootAngles, transformArray);
        magnitudeDict = MyUtils.Array2Dict<string, float>(new string[] { "0", "1", "2" }, magnitudeArray);

        for (int i = 0; i < tmpBlendTreePositionList.Count / 2; i++)
        {
            blendTreePositionList.Add(new Vector2(tmpBlendTreePositionList[2 * i], tmpBlendTreePositionList[2 * i + 1]));
        }

    }

    protected override void SpecificInitialize()
    {
        lastDraw = Vector2.zero;
        isFirstFrameDrawing = false;
        loadPixelPosition = Vector2.zero;
    }

    public override void SpecificOnTouchBegan(Touch touch)
    {
        lastDraw = Vector2.zero;
    }

    public override void SpecificOnTouchMoved(Touch touch)
    {
        int tagHash = animator.GetCurrentAnimatorStateInfo(0).tagHash;
        if (tagHash == drawAnimatorTagHash)
        {
            DrawInputHandle(touch);
        }
        else
        {
            ProcessStandardTouchMoved(touch);
        }
    }

    public override void SpecificOnTouchEnded(Touch touch)
    {
        if (animator.GetCurrentAnimatorStateInfo(0).tagHash == drawAnimatorTagHash)
        {
            ShootArrow();
        }
        else
        {
            LoadArrow(touch);
        }
    }

    // TODO; -4はuglyすぎるからBlendTreeの方をちゃんとしよ
    private void DrawInputHandle(Touch touch)
    {
        if (isFirstFrameDrawing)
        {
            loadPixelPosition = touch.position;
            isFirstFrameDrawing = false;
        }
        Vector2 draw = touch.position - loadPixelPosition;
        if (isLoggingEnabled) Debug.Log("draw: " + draw);

        draw = draw / maxDrawPixelDeltaPositionMagnitude;


        // 
        float projection = Vector2.Dot(draw, transform.right);
        if (projection > 0.25f)
        { // 引いてるときに前にスワイプでキャンセルして動かせる
            StartCoroutine(ShootTransitionCoroutine(transitionDict["Ready"], Vector2.zero));
            ProcessStandardTouchMoved(touch);
            return;
        }

        draw = -draw;
        draw.x = transform.right[0] * draw.x;

        lastDraw = draw;

        animator.SetFloat(horizontal_drawAnimatorParamHash, draw.x);
        animator.SetFloat(vertical_drawAnimatorParamHash, draw.y);

        string clipName = animator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
        if (isLoggingEnabled) Debug.Log(clipName);
    }

    private void LoadArrow(Touch touch)
    {
        isFirstFrameDrawing = true;

        StartCoroutine(PulseAnimatorParamBoolCoroutine(loadAnimatorParamHash));

        animator.SetFloat(horizontal_drawAnimatorParamHash, 0f);
        animator.SetFloat(vertical_drawAnimatorParamHash, 0f);
    }

    private void ShootArrow()
    {
        string clipName = NearestClip();
        if (isLoggingEnabled) Debug.Log(clipName);
        string[] words = clipName.Split('_'); // 0:arrow, 1:{Ready, Draw}, 2:{0, 45}, 3:{0, 1, 2}
        string stateStr = words[1];
        string directionStr = words[2];
        string drawStr = words[3];


        if (stateStr == "Ready")
        { // 何もせずに抜ける
            StartCoroutine(ShootTransitionCoroutine(transitionDict["Ready"], Vector2.zero));
            return;
        }
        else
        { // 引き方によって速度とdirection変える
            Vector2 v0 = magnitudeDict[drawStr] * directionDict[directionStr];
            v0.x = transform.right.x * v0.x;
            StartCoroutine(ShootTransitionCoroutine(transitionDict[directionStr], v0, directionStr, drawStr));

        }
    }

    private string NearestClip()
    {
        if (isLoggingEnabled) Debug.Log("lastDraw: " + lastDraw);
        // argmax
        float minMagnitude = 100f;
        int i_min = 0;
        if (isLoggingEnabled) Debug.Log("blendTreePositionList.Count: " + blendTreePositionList.Count);
        for (int i = 0; i < blendTreePositionList.Count; i++)
        {
            float dist = (lastDraw - blendTreePositionList[i]).magnitude;
            if (minMagnitude > dist)
            {
                minMagnitude = dist;
                i_min = i;
            }
            if (isLoggingEnabled) Debug.Log("" + i + blendTreeClipNameList[i] + dist + minMagnitude + i_min);
        }

        return blendTreeClipNameList[i_min];
    }

    private IEnumerator ShootTransitionCoroutine(int hashTransision, Vector2 v0, string directionStr = "", string drawStr = "")
    {
        if (hashTransision != 1)
        {
            if (PhotonNetwork.connected)
            {
                currentArrow = (GameObject)PhotonNetwork.Instantiate(arrowPrefab.name,
                    transformDict[directionStr].position,
                    Quaternion.FromToRotation(Vector3.right, v0.normalized), 0);
            }
            else
            {
                currentArrow = (GameObject)Instantiate(arrowPrefab, Vector3.zero, Quaternion.identity);
                currentArrow.GetComponent<PhotonTransformView>().enabled = false;
                currentArrow.transform.position = transformDict[directionStr].position;
                currentArrow.transform.rotation = Quaternion.FromToRotation(Vector3.right, v0.normalized);
                if (agent != null)
                {
                    agent.ShootReward(drawStr);
                }
            }
            currentArrow.GetComponent<ArrowController>().velocity = v0;
            currentArrow.GetComponent<HitboxManager>().myPlayerController = this;
        }

        // attackをonにして、1frame後にoffにする
        animator.SetInteger(shootAnimatorParamHash, hashTransision);

        yield return StartCoroutine(MyUtils.WaitForSecondsFixed(0.1f));

        animator.SetInteger(shootAnimatorParamHash, 0);
    }
}
