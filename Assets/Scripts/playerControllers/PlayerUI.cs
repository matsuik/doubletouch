﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerUI : MonoBehaviour, ITouchable
{
    [HideInInspector, System.NonSerialized]
    public PlayerController playerController;
    // set by playerController itself by Find

    public Text damageText;
    // set in prefab

    public GameObject spriteRendererObject;
    // set in prefab

    void Start()
    {
        damageText.text = "0%";
    }

    public void UpdateDamage(float damage)
    {
        damageText.text = $"{(int)damage}%";
    }

    public void SetSprite(SpriteRenderer playerSpriteRenderer)
    {
        var spriteRenderer = spriteRendererObject.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.sprite = playerSpriteRenderer.sprite;
            spriteRenderer.color = playerSpriteRenderer.color;
        }
        else
        {
            Debug.LogError("spriteRenderer is null");
        }
    }

    public void OnTouchBegan(Touch touch)
    {
    }

    public void OnTouchMoved(Touch touch)
    {
        Vector2 touchWorldPosition = Camera.main.ScreenToWorldPoint(touch.position);
        spriteRendererObject.transform.position = touchWorldPosition;
    }

    public void OnTouchEnded(Touch touch)
    {
        var col = TouchController.GetNearestCol(touch);
        if (col != null)
        {
            if (col.CompareTag("Player"))
            {
                if (col.gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Idle"))
                {
                    PlayerController pcActive = col.gameObject.GetComponent<PlayerController>();
                    if (!playerController.isFinished && !playerController.gameObject.activeInHierarchy)
                    {
                        playerController.Transit(col.gameObject.transform, pcActive.currentImpact);
                        col.gameObject.SetActive(false);
                    }
                }
            }
        }
        spriteRendererObject.transform.position = transform.position;
    }

    public bool CanTouch()
    {
        return (!playerController.isFinished && !playerController.gameObject.activeInHierarchy);
    }
}
