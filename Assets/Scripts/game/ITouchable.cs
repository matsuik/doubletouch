﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITouchable
{
    void OnTouchBegan(Touch touch);

    void OnTouchMoved(Touch touch);

    void OnTouchEnded(Touch touch);

    bool CanTouch();
}
