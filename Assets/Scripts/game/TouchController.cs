﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class TouchController : MonoBehaviour
{
    public static float touchCircleRadius = 0.7f;

    public bool isLogEnabled;

    public bool toShowGizmo;

    public GameObject sphere;

    private Vector2 touchWorldPosition = new Vector2(1000f, 1000f);

    private ITouchable currentTouchable;
    private Dictionary<int, ITouchable> touchGoDict;

    public static Collider2D[] overlapColliderArray = new Collider2D[4];

    [HideInInspector]
    public Queue<Touch> inputQueue = new Queue<Touch>();

    [HideInInspector]
    public Touch? repeatInput = null;

    void Start()
    {
        touchGoDict = new Dictionary<int, ITouchable>();
        if (toShowGizmo)
        {
            sphere.SetActive(false);
        }
    }

    void FixedUpdate()
    {
        if (inputQueue.Count == 0 && repeatInput != null)
        {
            ProcessTouch((Touch)repeatInput);
        }
        else
        {
            while (inputQueue.Count > 0)
            {
                if ((inputQueue.Peek().phase == TouchPhase.Moved || inputQueue.Peek().phase == TouchPhase.Stationary) && inputQueue.Count == 1)
                {
                    repeatInput = inputQueue.Dequeue();
                    ProcessTouch((Touch)repeatInput);
                }
                else
                {
                    ProcessTouch(inputQueue.Dequeue());
                }
            }
        }

    }

    void Update()
    {
        repeatInput = null;
        foreach (Touch touch in Input.touches)
        {
            inputQueue.Enqueue(touch);
        }
    }

    void ProcessTouch(Touch touch)
    {
        switch (touch.phase)
        {
            case TouchPhase.Began:
                if (toShowGizmo)
                {
                    touchWorldPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    sphere.SetActive(true);
                    sphere.transform.position = new Vector3(touchWorldPosition.x, touchWorldPosition.y, 0f);
                }

                var nearest_col = GetNearestCol(touch);
                if (nearest_col != null)
                {
                    currentTouchable = (ITouchable)nearest_col.gameObject.GetComponent(typeof(ITouchable));
                    currentTouchable.OnTouchBegan(touch);
                    touchGoDict.Add(touch.fingerId, currentTouchable);
                }
                break;

            case TouchPhase.Stationary:
            case TouchPhase.Moved:
                if (toShowGizmo)
                {
                    touchWorldPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    sphere.transform.position = new Vector3(touchWorldPosition.x, touchWorldPosition.y, 0f);
                }

                if (touchGoDict.TryGetValue(touch.fingerId, out currentTouchable))
                {
                    currentTouchable.OnTouchMoved(touch);
                }
                break;

            case TouchPhase.Ended:
                if (toShowGizmo)
                {
                    touchWorldPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    sphere.SetActive(false);
                }

                if (touchGoDict.TryGetValue(touch.fingerId, out currentTouchable))
                {
                    currentTouchable.OnTouchEnded(touch);
                    touchGoDict.Remove(touch.fingerId);
                }
                break;
        }
    }

    void OnDrawGizmos()
    {
        if (toShowGizmo)
        {
            Gizmos.DrawWireSphere((Vector3)touchWorldPosition, touchCircleRadius);
        }
    }

    public static Collider2D GetNearestCol(Touch touch)
    {
        var touchWorldPosition = Camera.main.ScreenToWorldPoint(touch.position);
        int overlapCount = Physics2D.OverlapCircleNonAlloc(touchWorldPosition, touchCircleRadius,
                                       overlapColliderArray, LayerMask.GetMask("Player", "UI"));

        float maxDist = 100f;
        Collider2D nearest_col = null;
        for (int i = 0; i < overlapCount; i++)
        {
            Collider2D col = overlapColliderArray[i];
            var touchable = (ITouchable)col.gameObject.GetComponent(typeof(ITouchable));
            if (touchable.CanTouch())
            {
                float distance = (col.gameObject.transform.position - (Vector3)touchWorldPosition).magnitude;
                if (distance <= maxDist)
                {
                    nearest_col = col;
                    maxDist = distance;
                }
            }
        }

        return nearest_col;
    }

}
