﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyUtils
{

    static public Dictionary<Key, Value> List2Dict<Key, Value>(IList<Key> keyList, IList<Value> valueList)
    {
        var dict = new Dictionary<Key, Value>();
        for (int i = 0; i < keyList.Count; i++)
        {
            dict.Add(keyList[i], valueList[i]);
        }
        return dict;
    }

    static public Dictionary<Key, Value> Array2Dict<Key, Value>(Key[] keyList, Value[] valueList)
    {
        var dict = new Dictionary<Key, Value>();
        for (int i = 0; i < keyList.Length; i++)
        {
            dict.Add(keyList[i], valueList[i]);
        }
        return dict;
    }

    public static int HashAnimatorParam(this Animator animator, string paramName)
    {
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.name == paramName)
            {
                return Animator.StringToHash(paramName);
            }
        }
        Debug.LogError("Animator of " + animator.gameObject.name + "doesnt have a param " + paramName);
        return 0;
    }

    public static IEnumerator WaitForSecondsFixed(float timeToWait)
    {
        float startTime = Time.time;
        while (true)
        {
            yield return new WaitForFixedUpdate();
            if (Time.time > startTime + timeToWait)
            {
                break;
            }
        }
    }

}
