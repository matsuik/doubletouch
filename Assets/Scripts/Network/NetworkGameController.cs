﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkGameController : Photon.PunBehaviour
{

    public bool friendlyFire;

    private GameObject restartButton;
    private GameObject leaveButton;
    public bool isLogEnabled = false;

    public GameObject playerPrefab1;
    public GameObject playerPrefab2;

    public Transform[] spawnPointsPlayer1 = new Transform[2];
    public Transform[] spawnPointsPlayer2 = new Transform[2];


    public static NetworkGameController gameController;
    #region MonoBehaviour
    void Awake()
    {
        gameController = this;
    }

    void Start()
    {
        var buttonName = "GameUI/Canvas/Network Restart Button";
        restartButton = GameObject.Find(buttonName);
        if (restartButton == null)
        {
            Debug.LogError($"CANNOT FIND {buttonName}");
        }
        restartButton.SetActive(false);

        buttonName = "GameUI/Canvas/Leave Room Button";
        leaveButton = GameObject.Find(buttonName);
        if (leaveButton == null)
        {
            Debug.LogError($"CANNOT FIND {buttonName}");
        }
        leaveButton.SetActive(false);

        Debug.LogFormat("Player id on Start: {0}", PhotonNetwork.player.ID);

        switch (PhotonNetwork.player.ID % 2)
        {
            case 0:
                PhotonNetwork.Instantiate(playerPrefab1.name, spawnPointsPlayer1[0].position, spawnPointsPlayer1[0].rotation, 0);
                PhotonNetwork.Instantiate(playerPrefab2.name, spawnPointsPlayer1[1].position, spawnPointsPlayer1[1].rotation, 0);
                break;
            case 1:
                PhotonNetwork.Instantiate(playerPrefab1.name, spawnPointsPlayer2[0].position, spawnPointsPlayer2[0].rotation, 0);
                PhotonNetwork.Instantiate(playerPrefab2.name, spawnPointsPlayer2[1].position, spawnPointsPlayer2[1].rotation, 0);
                break;
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (this.isActiveAndEnabled)
        {
            if (isLogEnabled) Debug.Log("boundary exit");

            other.gameObject.SetActive(false);

            if (other.CompareTag("Player"))
            {
                other.gameObject.GetComponent<PlayerController>().isFinished = true;
                GameOver();
            }
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 20), PhotonNetwork.player.ID.ToString());
    }


    #endregion


    #region PUN

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom is called");
        SceneManager.LoadScene(0);
    }

    //	public override void OnPhotonPlayerConnected(PhotonPlayer other)
    //	{
    //		PhotonNetwork.LoadLevel (1);
    //	}


    #endregion


    #region Custum

    // UI : Leave Room Button
    public void LeaveRoom()
    {
        Debug.Log("UI: LeaveRoom is called");
        PhotonNetwork.LeaveRoom();
    }

    void GameOver()
    {
        restartButton.SetActive(true);
        leaveButton.SetActive(true);
    }

    // UI : Restart Button
    public void RestartGame()
    {
        photonView.RPC("LoadArena", PhotonTargets.All);
    }

    [PunRPC]
    void LoadArena()
    {
        Debug.Log("PhotonNetwork : Loading Level");
        PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);
        PhotonNetwork.LoadLevel(1);
    }

    #endregion

}
