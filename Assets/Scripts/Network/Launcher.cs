﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : Photon.PunBehaviour
{

    string gameVersion = "1912";


    public PhotonLogLevel loglevel = PhotonLogLevel.Informational;

    public GameObject playButton;
    public GameObject connectingLabel;

    #region MonoBehaviour

    void Awake()
    {
        PhotonNetwork.autoJoinLobby = false;
        PhotonNetwork.automaticallySyncScene = true;
        PhotonNetwork.logLevel = loglevel;
        PhotonNetwork.sendRate = 110;
        PhotonNetwork.sendRateOnSerialize = 110;
    }

    void Start()
    {
        playButton.SetActive(true);
        connectingLabel.SetActive(false);
        if (!PhotonNetwork.connected)
        {
            PhotonNetwork.ConnectUsingSettings(gameVersion);
            playButton.SetActive(false);
            connectingLabel.SetActive(true);
        }
    }

    #endregion

    #region Photon.PunBehaviour

    public override void OnConnectedToMaster()
    {
        playButton.SetActive(true);
        connectingLabel.SetActive(false);
        Debug.Log("DemoAnimator/Launcher: OnConnectedToMaster() was called by PUN");
    }


    public override void OnDisconnectedFromPhoton()
    {
        Debug.LogWarning("DemoAnimator/Launcher: OnDisconnectedFromPhoton() was called by PUN");
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug.Log("Dont care Operation failed: OperationResponse 225: ReturnCode: 32760 (No match found)\nIt means there is no room.");
        Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
        // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = 2 }, null);
    }

    public override void OnJoinedRoom()
    {
        connectingLabel.SetActive(false);
        Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        if (PhotonNetwork.room.PlayerCount == 1)
        {
            PhotonNetwork.LoadLevel(1);
        }
    }


    #endregion

    #region Custom

    // UI: Play Button
    public void Connect()
    {
        playButton.SetActive(false);
        connectingLabel.SetActive(true);
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings(gameVersion);
        }
    }

    #endregion

}
